

#include <QCoreApplication>

#include "helixserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    HelixServer *server = new HelixServer(1234);
    QObject::connect(server, &HelixServer::closed, &a, &QCoreApplication::quit);


    return a.exec();
}
