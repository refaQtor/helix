

#include "helixserver.h"


#include "QtWebSockets/qwebsocketserver.h"
#include "QtWebSockets/qwebsocket.h"
#include <QtCore/QDebug>


HelixServer::HelixServer(quint16 port, QObject *parent) :
    QObject(parent),
    m_pWebSocketServer(new QWebSocketServer(QStringLiteral("Helix Server"),
                                            QWebSocketServer::NonSecureMode, this)),
    m_clients()
{

}

HelixServer::~HelixServer()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
}

void HelixServer::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, &QWebSocket::textMessageReceived, this, &HelixServer::processTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &HelixServer::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &HelixServer::socketDisconnected);

    m_clients << pSocket;
}

void HelixServer::processTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient) {
        pClient->sendTextMessage(message);
    }
    m_pWebSocketServer->close();
}

void HelixServer::processBinaryMessage(QByteArray message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient) {
        pClient->sendBinaryMessage(message);
    }
}

void HelixServer::socketDisconnected()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient) {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}

