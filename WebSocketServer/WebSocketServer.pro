#-------------------------------------------------
#
# Project created by QtCreator 2015-01-19T17:35:06
#
#-------------------------------------------------

QT       += core websockets

QT       -= gui

TARGET = HelixServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    helixserver.cpp

HEADERS += \
    helixserver.h
