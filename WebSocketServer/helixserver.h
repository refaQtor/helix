

#ifndef HELIXSERVER_H
#define HELIXSERVER_H


#include <QtCore/QObject>
#include <QtCore/QList>
#include <QtCore/QByteArray>


QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)


class HelixServer : public QObject
{
    Q_OBJECT
public:
    explicit HelixServer(quint16 port, QObject *parent = Q_NULLPTR);
    ~HelixServer();

Q_SIGNALS:
    void closed();

private Q_SLOTS:
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void socketDisconnected();

private:
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
};


#endif // HELIXSERVER_H
