#-------------------------------------------------
#
# Project created by QtCreator 2015-01-19T20:42:00
#
#-------------------------------------------------

QT       += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimLauncher
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    helixclient.cpp

HEADERS  += mainwindow.h \
    helixclient.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
