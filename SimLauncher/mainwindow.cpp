// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at http://mozilla.org/MPL/2.0/.

// Copyright (c) 2015, Shannon Mackey refaQtor@gmail.com



#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDir>
#include <QFile>
#include <QStandardPaths>

#include "helixclient.h"




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    server(new QProcess())
{
    ui->setupUi(this);


    if (startHelixServer()) {
        HelixClient client(QUrl(QStringLiteral("ws://localhost:1234")));
      //  QObject::connect(&client, &HelixClient::closed, this, close());
    }
}

MainWindow::~MainWindow()
{
    server->close();
    server->deleteLater();
    delete ui;
}

bool MainWindow::startHelixServer()
{
    bool ok = false;
    QString local_app_path = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).at(0);
    QDir dir;
    ok = dir.mkdir(local_app_path);
    local_app_path.append("/HelixServer");
    QFile check(local_app_path);
    if (check.exists())
        check.remove();
    ok = QFile::copy(":/servers/resources/HelixServer",local_app_path);
    QFile exe(local_app_path);
    ok = exe.setPermissions(QFileDevice::ExeUser);
    server->start(local_app_path);
    return ok;
}
