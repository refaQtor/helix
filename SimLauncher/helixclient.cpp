// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at http://mozilla.org/MPL/2.0/.

// Copyright (c) 2015,  Shannon Mackey refaQtor@gmail.com



#include "helixclient.h"

#include <QtCore/QDebug>


HelixClient::HelixClient(const QUrl &url, QObject *parent) :
    QObject(parent),
    m_url(url)
{
    connect(&m_webSocket, &QWebSocket::connected, this, &HelixClient::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &HelixClient::closed);
    m_webSocket.open(QUrl(url));
}

HelixClient::~HelixClient()
{
   m_webSocket.close();
}

void HelixClient::onConnected()
{
    qDebug() << "WebSocket connected";
    connect(&m_webSocket, &QWebSocket::textMessageReceived,
            this, &HelixClient::onTextMessageReceived);
    m_webSocket.sendTextMessage(QStringLiteral("Helix Client"));
}

void HelixClient::onTextMessageReceived(QString message)
{
    qDebug() << "Message received:" << message;

}
