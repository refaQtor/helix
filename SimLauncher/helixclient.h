// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// You can obtain one at http://mozilla.org/MPL/2.0/.

// Copyright (c) 2015, Shannon Mackey refaQtor@gmail.com


#ifndef HELIXCLIENT_H
#define HELIXCLIENT_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>

class HelixClient : public QObject
{
    Q_OBJECT
public:
    explicit HelixClient(const QUrl &url, QObject *parent = Q_NULLPTR);

    ~HelixClient();
Q_SIGNALS:
    void closed();

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);

private:
    QWebSocket m_webSocket;
    QUrl m_url;
};
#endif // HELIXCLIENT_H
